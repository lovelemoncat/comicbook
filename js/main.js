(function (win) {


    win.stageBuiler= {
        //当前场景的执行的索引
        sceneIndex : 0,
        /**
         * 初始化新建舞台，并执行第一个场景
         */
        init: function () {
            //第一步： 创建舞台
            this.stage = new Konva.Stage({
                container: 'container', //设置当前舞台的容器
                width: constant.width,//设置舞台的宽高全屏
                height: constant.height
            });
            // 场景的构造器
            this.sceneBuilders = [FirstScene, SecondScene,ThirdScene,FourthScene,FifthScene];
            //上来之后执行第一个场景
            this.sceneBuilders[0]().play();
        },
        /**
         * 切换上一场景
         */
        preScene: function () {
            //把当前执行场景的索引-1
            //下滑动 执行上一个场景 的play()
            this.sceneIndex = this.sceneIndex <= 0 ? 0 : this.sceneIndex - 1;
            //(),调用场景函数时，会返回new Scene. 构造中调用init();
            this.sceneBuilders[this.sceneIndex]().play();
        },
        /**
         * 切换下一场景
         */
        nextScene : function () {
            //下一个场景数大于，总场景数时，
            // 不超出把当前执行场景的索引 +1,
            this.sceneIndex = this.sceneIndex >= this.sceneBuilders.length - 1 ? this.sceneBuilders.length - 1 : this.sceneIndex + 1;
            //(),调用场景函数时，会返回new Scene. 构造中调用init();
            this.sceneBuilders[this.sceneIndex]().play();
        },
        /**
         * 给舞台添加 上滑动，和下滑动的事件
         */
        addStageEvent: function () {
            var startY = 0;
            var endY = 0;
            this.stage.on('contentMousedown contentTouchstart', function (e) {
                if (e.type == 'contentMousedown') {
                    // console.log(e.evt.screenX + ' ' +  e.evt.screenY);
                    startY = e.evt.screenY;

                } else if (e.type == 'contentTouchstart') {
                    // console.log(e.evt.touches[0].screenX + ' ' + e.evt.touches[0].screenY);
                    startY = e.evt.touches[0].screenY;
                }
                // console.log(e);
            });

            this.stage.on('contentMousemove contentTouchmove', function (e) {
                if (e.type == 'contentMousemove') {
                    // console.log(e.evt.screenX + ' ' +  e.evt.screenY);
                    endY = e.evt.screenY;
                } else if (e.type == 'contentTouchmove') {
                    // console.log(e.evt.touches[0].screenX + ' ' + e.evt.touches[0].screenY);
                    endY = e.evt.touches[0].screenY;
                }
                // console.log(e);
            });

            this.stage.on('contentMouseup contentTouchend', function (e) {
                if (endY > startY) {
                    win.stageBuiler.preScene();
                } else {
                    //执行下一个场景的 play();
                    win.stageBuiler.nextScene();
                }


            });
        }

    }
//========================================
    win.stageBuiler.init();
    /**
     * 给舞台添加滑动的事件
     */
   // win.stageBuiler.addStageEvent();
})(window)