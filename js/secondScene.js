//构造h5的场景
function SecondScene() {
    var bgLayer = new Konva.Layer();
    var animateLayer = new Konva.Layer({
        opacity:0
    });
    var lightLayer = new Konva.Layer();
    /**
     * ===============================
     */
        //背景
    var bgRect = new Konva.Rect({
        x: constant.x,
        y: constant.y,
        fill: '#5c8c71',
        width: constant.width,
        height: constant.height,
        stroke: 'white',
        strokeWidth: 0
    });
    //把百分比的文字放到 柱状图的顶部
    var mainTitle = new Konva.Text({
        x:  0,
        y: 1/4 * constant.height-20,
        fontSize: 40,
        text: '糖鼠和米鼠',
        fill: "white",
        width: constant.width,// 配合让文字居中
        align: 'center',  //
        strokeWidth:1,
        opacity:5,
        fontFamily:'微软雅黑'
    });
    var author = new Konva.Text({
        x:  0,
        y: 1/4 * constant.height+40,
        fontSize: 20,
        text: '郑渊洁／原著 水母咪／绘',
        fill: "white",
        width: constant.width,// 配合让文字居中
        align: 'center',  //
        strokeWidth:1,
        opacity:5,
        fontFamily:'kaiti'
    });
    /**
     * 老鼠
     * @type {{idle: number[]}}
     */
    var miceAnimations = {
        idle: [
            0, 0, 200, 273,
            200, 0, 200, 273,
            400, 0, 200, 273,
        ]
    };
    var mice = new Konva.Sprite({
        x: constant.centX-50,
        y: 2/5 * constant.height,
        scale:{x:0.5,y:0.5},
        image: constant.tu["mice"],
        animation: 'idle',
        animations: miceAnimations,
        frameRate: 7,
        frameIndex: 0,
    });


    /**
     * 爬动的小孩
     * @type {{idle: number[]}}
     */
    var babyanimations = {
        idle: [
            0, 0, 182, 149,
            183, 0, 182, 149,
            365, 0, 182, 149,
            547, 0, 182, 149,
        ]
    };
    var baby = new Konva.Sprite({
        x:0,
        y: 3/4 * constant.height,
        scale:{x:0.5,y:0.5},
        image: constant.tu["baby"],
        animation: 'idle',
        animations: babyanimations,
        frameRate: 7,
        frameIndex: 0,
    });

    return new MyScene({
        name: '场景2',
        layers: [bgLayer, animateLayer, lightLayer], //最后的层放到最上面
        stage: stageBuiler.stage,
        init: function() {

            //初始化场景中的所有形状
            bgLayer.add(bgRect);
            animateLayer.add(mainTitle);
            animateLayer.add(author);
            animateLayer.add(mice);
            animateLayer.add(baby);

            this.layers.forEach(function(layer) {
                layer.draw();
            });
        },
        pre: function() {

            animateLayer.to({
                opacity:1,
                duration:2,
                easing: Konva.Easings.EaseIn,
                onFinish: function () {
                    mice.start();
                    baby.start();
                    baby.to({
                        x:constant.width,
                        duration:3,
                        onFinish: function () {
                            stageBuiler.nextScene();
                        }
                    })
                }
             })
        },
        post: function( dopre ) {
            var self = this;
            self.layers.forEach(function (item) {
                item.destroy();//把所有层销毁
            });
            dopre(); //必须执行next方法，执行下一个场景的初始化和入场动画
        }
    });
}