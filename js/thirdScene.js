//构造h5的场景
function ThirdScene() {
    var bgLayer = new Konva.Layer();
    var animateLayer = new Konva.Layer();
    var lightLayer = new Konva.Layer();
    /**
     * ===============================
     */
        //背景
    var bgRect = new Konva.Rect({
        x: constant.x,
        y: constant.y,
        fill: '#F4F0D7',
        width: constant.width,
        height: constant.height,
        stroke: 'white',
        strokeWidth: 0
    });
    var dialogue = new Konva.Text({
        x:  constant.width,
        y: 2/8 * constant.height,
        fontSize: 20,
        text: '他最爱吃糖，自称糖鼠。',
        strokeWidth:0.1,
        stroke:"white",
        fill: "#458C59",
        width: constant.width,// 配合让文字居中
        align: 'center',  //
        fontFamily:'kaiti'
    });


    var hand = new Konva.Image({
        x:80,
        y: 3/4 * constant.height,
        image: constant.tu["hand"],
        opacity:0
    });
    /**
     * 第三页图像（）
     * @type {Konva.Image}
     */
    var page3 = new Konva.Image({
        x: -constant.width,
        y: constant.height-1/2*constant.height,
        image: constant.tu["page3"],
        width:(1/2*constant.height)/constant.tu["page3"].height*constant.tu["page3"].width,
        height:1/2*constant.height,
    });
    /**
     * 第三页图像（糖）
     * @type {Konva.Image}
     */
    var sugar = new Konva.Image({
        x: constant.centX,
        y: -constant.tu["sugar"].height/2,
        image: constant.tu["sugar"],
        width:constant.tu["sugar"].width/2,
        height:constant.tu["sugar"].height/2,
    });
    /**
     * 老鼠
     * @type {Konva.Image}
     */
    var mice = new Konva.Image({
        x: constant.width-(3/4*constant.height)/constant.tu["page3mice"].height*constant.tu["page3mice"].width,
        y: 0,
        opacity:1,
        image: constant.tu["page3mice"],
        width:(3/4*constant.height)/constant.tu["page3mice"].height*constant.tu["page3mice"].width,
        height:3/4*constant.height,

    });
    /**
     * 爬动的小孩
     * @type {{idle: number[]}}
     */
    var babyanimations = {
        idle: [
            0, 0, 182, 149,
            183, 0, 182, 149,
            365, 0, 182, 149,
            547, 0, 182, 149,
        ]
    };
    var baby = new Konva.Sprite({
        x:-constant.tu["baby"].width/8,
        y: 3/4 * constant.height,
        scale:{x:0.5,y:0.5},
        image: constant.tu["baby"],
        animation: 'idle',
        animations: babyanimations,
        frameRate: 7,
        frameIndex: 0,
        draggable: true
    });
    /**
     * 绑定拖动事件
     */
    var isFirst =true;
    var y = 0;
    var step =0;
    tool.event.dragEvent(baby, function (pos) {
        //console.log("开始"+baby.x());
        y = baby.y();
        /**
         * 拖动开始入画
         */
       if(isFirst){
           //图像入画（从左到右）
           page3.to({
               x:0,
               duration:2
           })
           //文字入画（从右到左）
           dialogue.to({
               x:0,
               duration:2
           })
       };
        /**
         * 3秒后
         */
        setTimeout(function () {
            isFirst =false;
            /**
            * 图像出画（从左到右）
            */
            page3.to({
                x:-constant.width,
                duration:1
            });
            /**
            * 文字出画（从右到左）
            */
            dialogue.to({
                x:-constant.width,
                duration:1
            });
            /**
             * 糖掉落
             */
            sugar.to({
                y:3/4*constant.height,
                duration:1,
                easing:Konva.Easings.StrongEaseIn,
                /**
                 * 第2句对话
                 */
                onFinish: function () {
                    dialogue.align("left");
                    dialogue.width(2/3*constant.width);
                    dialogue.text("这天，他吃惊地发现一颗牛奶糖的包装纸上也有一只老鼠！");
                    dialogue.to({
                        x:1/8*constant.width,
                        duration:1,
                        /**
                         * 小孩出画
                         */
                        onFinish: function () {
                            baby.to({
                                x:constant.width,
                                duration:2,
                                /**
                                 * 切换下一场景
                                 */
                                onFinish: function () {
                                   stageBuiler.nextScene();
                                }

                            });

                        }
                    })
                }
            });
        }, 4000 );
    },function (pos) {
        //console.log("移动"+baby.x());
        baby.y(y);
        /**
         * 不能拖出屏
         * @type {number}
         */
        step = constant.width-(constant.tu["baby"].width/8+baby.x());
        if(step<0){
            baby.x(constant.width-constant.tu["baby"].width/8);
        }else if(step>constant.width-constant.tu["baby"].width/8){
            baby.x(0);
        }
    },function (pos) {
        //console.log("松开"+baby.x());
    });

    return new MyScene({
        name: '场景3',
        layers: [bgLayer, animateLayer, lightLayer], //最后的层放到最上面
        stage: stageBuiler.stage,
        init: function() {

            //初始化场景中的所有形状
            bgLayer.add(bgRect);
            animateLayer.add(page3);
            animateLayer.add(dialogue);
            animateLayer.add(mice);
            animateLayer.add(sugar);

            animateLayer.add(baby);
            animateLayer.add(hand);
            baby.start();

            this.layers.forEach(function(layer) {
                layer.draw();
            });
        },
        pre: function() {
            /**
             * 左屏外，进入
             */
            baby.to({
                x:0,
                duration:1,
                onFinish: function () {
                    /**
                     * 小手滑动
                     */
                    hand.opacity(1);
                    hand.to({
                        x:150,
                        duration:1,
                        /**
                         * 小手二次滑动
                         */
                        onFinish: function () {
                            hand.x(80);
                            hand.to({
                                x:150,
                                duration:1,
                                onFinish: function () {
                                    hand.opacity(0);
                                }
                            });
                        }
                    });
                }
            });

        },
        post: function( dopre ) {

            var self = this;

            self.layers.forEach(function (item) {
                item.destroy();//把所有层销毁
            });
            dopre(); //必须执行next方法，执行下一个场景的初始化和入场动画
        }
    });
}