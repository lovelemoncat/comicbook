/**
 * Created by Administrator on 2017/6/19 0019.
 */
/**
 * 工具类
 */
(function (win) {
    win.tool = {
        version: 1.0
    };
    /**
     * 属性继承
     * @param target
     * @param source
     * @returns {*}
     */
    tool.extend = function (target, source) {
        for (var key in source) {
            if (source.hasOwnProperty(key)) {
                target[key] = source[key];
            }
        }
        return target;
    };
    /**
     *
     * @param parentURl 图片父目录
     * @param nameArr 图片名数组
     * @return {
     *    图片名:图片对象
     * }
     */
    tool.loadImg = function (parentURl, nameArr,fn) {
        var totalNum = nameArr.length;
        var tu = {};
        for(var i =0;i<nameArr.length;i++){
            tool.loadOneImg(parentURl,nameArr[i], function (name,obj) {
                /**
                 * key:图片名，
                 * value:图片对象
                 */
                tu[name] = obj;
                /**
                 * 最后一张图片加载完成后，返回
                 */
                if(--totalNum == 0){
                    fn(tu);
                }
            })
        }
    };
    /**
     * 加载一张图片
     * @param parentURl
     * @param imgName
     * @param fn
     */
    tool.loadOneImg = function (parentURl,imgName,fn) {
        var imageObj = new Image();
        imageObj.src = parentURl + imgName;
        imageObj.onload = function () {
            var name = imgName.substring(0, imgName.indexOf("."));
           fn(name,imageObj);
        }
    };
    tool.anim = {
        /**
         * 自定义动画，按秒回调
         * @param fn 回调方法
         * @param perSeconds 0-1之音  0.5为一秒2次
         * @param layer 应用的动画层
         */
        perSeconds: function (fn,perSeconds,layer) {
            var velocity = 10;
            var total =0;
            var anim = new Konva.Animation(function(frame) {
                total += velocity * (frame.timeDiff / 1000);
                if(total>velocity*perSeconds){
                    //回调
                    fn(anim);
                    total=0;
                }
            },layer);
            anim.start();
        }
    };
    tool.event = {
        /**
         * 拖动
         * @param obj 绑定事件的元素
         * @param startFn 按下回调
         * @param moveFn 按拖动回调
         * @param upFn 松开回调
         */
        dragEvent: function (obj,startFn,moveFn,upFn) {

           /* var pos ={
                startX : 0,
                startY : 0,
                moveX:0,
                moveY:0,
                betweenX :0,
                betweenY :0
            }*/
            obj.on('dragstart', function (e) {
               /* if (e.type == 'mousedown') {
                    // console.log(e.evt.screenX + ' ' +  e.evt.screenY);
                    pos.startX = e.evt.screenX;
                    pos.startY = e.evt.screenY;

                } else if (e.type == 'touchstart') {
                    // console.log(e.evt.touches[0].screenX + ' ' + e.evt.touches[0].screenY);
                    pos.startX = e.evt.touches[0].screenY;
                    pos.startY = e.evt.touches[0].screenX;
                }*/
                //回调开始坐标
                startFn(e);
            });

            obj.on('dragmove', function (e) {
               /* if (e.type == 'mousemove') {
                    // console.log(e.evt.screenX + ' ' +  e.evt.screenY);
                    pos.moveX = e.evt.screenX;
                    pos.moveY = e.evt.screenY;
                } else if (e.type == 'touchmove') {
                    // console.log(e.evt.touches[0].screenX + ' ' + e.evt.touches[0].screenY);
                    pos.moveX = e.evt.touches[0].screenX;
                    pos.moveY = e.evt.touches[0].screenY;
                }*/
                //回调开始坐标
                moveFn(e);
            });

            obj.on('dragend', function (e) {
                  /*  pos.betweenX = pos.moveX-pos.startX;
                   pos.betweenY =pos.moveY - pos.startY;*/
                //回调开始坐标
                upFn(e);
            });
        }
    }

})(window)
