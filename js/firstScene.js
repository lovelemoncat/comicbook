//构造h5的场景
function FirstScene() {
    var bgLayer = new Konva.Layer();
    var animateLayer = new Konva.Layer();
    var lightLayer = new Konva.Layer();
    /**
     * ========================蓝色背景
     * @type {Konva.Rect}
     */
    var bgRect = new Konva.Rect({
        x: constant.x,
        y: constant.y,
        fill: '#5c8c71',
        width: constant.width,
        height: constant.height,
        stroke: 'white',
        strokeWidth: 0
    });

    var animations = {
        idle: [
            0, 0, 182, 149,
            183, 0, 182, 149,
            365, 0, 182, 149,
            547, 0, 182, 149,
        ]
    };
    //把百分比的文字放到 柱状图的顶部
    var mainTitle = new Konva.Text({
        x:  0,
        y: 1/4 * constant.height,
        fontSize: 40,
        text: '',
        fill: "#f8d2a4",
        width: constant.width,// 配合让文字居中
        align: 'center',  //
        strokeWidth:1,
        opacity:5,
        fontFamily:'kaiti'
    });
    //把百分比的文字放到 柱状图的顶部
    var onloadText = new Konva.Text({
        x:  0,
        y: 3/5 * constant.height,
        fontSize: 20,
        text: '正在加载',
        fill: "#f8d2a4",
        width: constant.width,// 配合让文字居中
        align: 'center',  //
        strokeWidth:1,
        opacity:5,
        fontFamily:'kaiti'
    });
    return new MyScene({
        name: '场景1',
        //在myScene中stage.add(所有层)，现在只需把元素添加到层上
        layers: [bgLayer, animateLayer, lightLayer], //最后的层放到最上面
        stage: stageBuiler.stage,
        init: function () {
            //初始化场景中的所有形状（元素添加到层上）
            bgLayer.add(bgRect);
            animateLayer.add(mainTitle);
            animateLayer.add(onloadText);



            this.layers.forEach(function (layer) {
                layer.draw();
            });


        },
        pre: function () {
            /**
             * 标题动画
             * @type {string}
             */
            var m_t= "漫游童话";
            var m_fs = [80,60,100,80,80];
            var m_c = ["#FCD209","#FF5D05","#F3976C","#f8d2a4","#FF7D9C"];
            //正在加载提示
            var onload_t= "正在加载";
            var index =0;
            tool.anim.perSeconds(function (anim) {
                mainTitle.fontSize(m_fs[index]);
                mainTitle.fill(m_c[index]);
                mainTitle.text(m_t.charAt(index++));

                onloadText.text(onloadText.text()+".");

                if(index ==5){
                    onloadText.text(onload_t);

                    //anim.stop();
                    mainTitle.text("绘本系列");
                    mainTitle.fontSize(40);
                    index=0;
                }
            },0.6,animateLayer);

            /**
             * 小孩动画
             */
            tool.loadOneImg("img/", "baby.png", function (name, imageObj) {
                //存加载动画的资源文件
                constant.tu[name]=imageObj;

                //新建爬动的小孩
                var baby = new Konva.Sprite({
                    x:0,
                    y: 2/5 * constant.height,
                    scale:{x:0.5,y:0.5},
                    image: imageObj,
                    animation: 'idle',
                    animations: animations,
                    frameRate: 7,
                    frameIndex: 0,

                });
                //（元素添加到层上）
                animateLayer.add(baby);
                //精灵动画启动
                baby.start();
                //位移动画
                baby.to({
                    x:constant.centX+40,
                    duration:2,

                    onFinish: function () {
                        baby.to({
                            easing: Konva.Easings.ElasticEaseIn,
                            rotation:-368,
                            x:constant.centX-50,
                            duration:3,
                            draggable:true
                        })

                    }
                })
            });
            /**
             * 加载资源文件
             */
            //
            var startTime=new Date();


            tool.loadImg("img/", ["titlePage.png","mice.png","hand.png","page3.png","sugar.png","page3mice.png","machine.png","cylinder.png","rect.png","mice5.png","bus.png","xuan.png"], function (tu) {
                tool.extend(constant.tu,tu);

                var endTime=new Date();


               if(endTime-startTime>=5500){
                   //alert("大于");
                    stageBuiler.nextScene();
                }else{
                  //;stageBuiler.nextScene()
                    setTimeout(function(){stageBuiler.nextScene()}, 5500-(endTime-startTime));
                }

            });




        },
        post: function (dopre) {

            var self = this;
            self.layers.forEach(function (item) {
                item.destroy();//把所有层销毁
            });
            dopre(); //必须执行next方法，执行下一个场景的初始化和入场动画


        }
    });
}