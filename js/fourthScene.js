//构造h5的场景
function FourthScene() {
    var bgLayer = new Konva.Layer();
    var animateLayer = new Konva.Layer();
    var lightLayer = new Konva.Layer();
    /**
     * ===============================
     */
    var content =["“喂，老弟，你不喜欢吃糖？”糖鼠惊讶地问。“我是米鼠。”糖纸上的老鼠说。",
        "糖鼠把糖吃光了。米鼠说：“我住在糖果厂里，那里有许多我的弟兄。”"
        ,"糖鼠羡慕极了：那日子，该多甜呀！”","手够快呀，宝宝"];
        //背景
    var bgRect = new Konva.Rect({
        x: constant.x,
        y: constant.y,
        fill: '#EDC054',
        width: constant.width,
        height: constant.height,
        stroke: 'white',
        strokeWidth: 0
    });

    /**
     * 机器
     * @type {Konva.Image}
     */
    var machine = new Konva.Image({
        x: constant.width-(3/4*constant.height)/constant.tu["machine"].height*constant.tu["machine"].width,
        y: 0,
        opacity:1,
        image: constant.tu["machine"],
        width:(3/4*constant.height)/constant.tu["machine"].height*constant.tu["machine"].width,
        height:3/4*constant.height,

    });
    /**
     * 机器
     * @type {Konva.Image}
     */
    var cylinder = new Konva.Image({
        x: constant.width-(3/4*constant.height)/constant.tu["machine"].height*constant.tu["machine"].width-(3/4*constant.height)/constant.tu["cylinder"].height*constant.tu["cylinder"].width,
        y: 0,
        opacity:1,
        image: constant.tu["cylinder"],
        width:(3/4*constant.height)/constant.tu["cylinder"].height*constant.tu["cylinder"].width,
        height:3/4*constant.height,

    });

    /**
     * 平台
     * @type {Konva.Image}
     */
    var rect = new Konva.Image({
        x: constant.width,
        y: constant.height-2/4*constant.height,
        opacity:1,
        image: constant.tu["rect"],
       /* width:(3/4*constant.height)/constant.tu["rect"].height*constant.tu["rect"].width,
        height:3/4*constant.height,*/
        width:(1/4*constant.height)/constant.tu["rect"].height*constant.tu["rect"].width,
        height:1/4*constant.height
    });
    /**
     * 糖
     * @type {Konva.Image}
     */
    var sugar = new Konva.Image({
        x: constant.width-(3/4*constant.height)/constant.tu["machine"].height*constant.tu["machine"].width-(3/4*constant.height)/constant.tu["cylinder"].height*constant.tu["cylinder"].width,
        y: 3/5*constant.height,
        image: constant.tu["sugar"],
        width:constant.tu["sugar"].width/2,
        height:constant.tu["sugar"].height/2,
        opacity:0,
        draggable: true
    });
    /**
     * 拖动
     */
    var cIndex =0;
    tool.event.dragEvent(sugar, function () {

    },function () {

    },function () {
         if(sugar.x()!=constant.width-(3/4*constant.height)/constant.tu["machine"].height*constant.tu["machine"].width-(3/4*constant.height)/constant.tu["cylinder"].height*constant.tu["cylinder"].width){

            if(cIndex===4){
                stageBuiler.nextScene();
            }else{
                dialogue.text(content[cIndex++]);
            }


         }
    });
    //背景
    var bgRect2 = new Konva.Rect({
        x: 1/8*constant.width,
        y: constant.y,
        fill: 'white',
        width: 6/8*constant.width,
        height: 1/3*constant.height,
        stroke: '#53A5A9',
        strokeWidth: 2,
        opacity:0.3,

    });
    var dialogue = new Konva.Text({
        x:  1/8 * constant.width,
        y: 1/8 * constant.height,
        fontSize: 20,
        text: '抢糖果，看故事',
        fill: "#458C59",
        width: 6/8 * constant.width,// 配合让文字居中
        align: 'center',  //
        fontFamily:'kaiti'
    });







    return new MyScene({
        name: '场景4',
        layers: [bgLayer, animateLayer, lightLayer], //最后的层放到最上面
        stage: stageBuiler.stage,
        init: function() {

            //初始化场景中的所有形状
            bgLayer.add(bgRect);
            animateLayer.add(rect);
            animateLayer.add(sugar);
            animateLayer.add(machine);
            animateLayer.add(cylinder);
            animateLayer.add(bgRect2);
            animateLayer.add(dialogue);

            this.layers.forEach(function(layer) {
                layer.draw();
            });
        },
        pre: function() {
            /**
             * 上下
             */
            tool.anim.perSeconds(function (anim) {
                /**
                 * 圆柱上下
                 */
                if(cylinder.y()==0){
                    cylinder.y(-100);
                    sugar.opacity(1);
                }else{
                    cylinder.y(0);
                    sugar.x(constant.width-(3/4*constant.height)/constant.tu["machine"].height*constant.tu["machine"].width-(3/4*constant.height)/constant.tu["cylinder"].height*constant.tu["cylinder"].width);
                    sugar.y(3/5*constant.height);
                    sugar.opacity(0);

                }
                /**
                 * 滚动平台
                 */
                if(rect.x()<-(1/4*constant.height)/constant.tu["rect"].height*constant.tu["rect"].width){
                    rect.x(constant.width);
                }else{
                    rect.to({
                        x:rect.x()-100
                    })
                }

            },1,animateLayer);

        },
        post: function( dopre ) {

            var self = this;

            self.layers.forEach(function (item) {
                item.destroy();//把所有层销毁
            });
            dopre(); //必须执行next方法，执行下一个场景的初始化和入场动画
        }
    });
}